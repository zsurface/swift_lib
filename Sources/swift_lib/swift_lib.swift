// create w element 1d array
func creatArray(w:Int)->[Int]{
    return [Int](repeating:0, count: w)
}

// create [h:height, w:width] 2d array
func creatArray(h:Int, w:Int)->[[Int]]{
    return [[Int]](repeating:[Int](repeatElement(0, count: w)), count: h)
}

// flip the column of [[Int]]
func flipColumn(arr:[[Int]])->[[Int]]{
    let heightNum = arr.count
    let widthNum = arr[0].count
    var tmpArr = [[Int]](repeating:[Int](repeatElement(0, count: widthNum)), count: heightNum)
    
    for w in 0..<widthNum {
        for h in  0..<heightNum {
            tmpArr[h][w] = arr[h][w]
        }
    }
    for w in 0..<widthNum {
        for h in 0..<heightNum/2 {
            var rh = heightNum - 1 - h
            var tmp = tmpArr[h][w]
            tmpArr[h][w] = tmpArr[rh][w]
            tmpArr[rh][w] = tmp
        }
    }
    return tmpArr
}

//   list 
class MyClass{
}
