import XCTest
@testable import swift_lib

final class swift_libTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
    }

    func testFlipColumn1(){
        let arr2d = [
            [0, 1, 1, 1],
            [0, 1, 1, 1],
            [0, 1, 1, 1],
            [1, 0, 1, 1],
            [1, 0, 1, 1]
        ]
        let flipArr2d = [
            [1, 0, 1, 1],
            [1, 0, 1, 1],
            [0, 1, 1, 1],
            [0, 1, 1, 1],
            [0, 1, 1, 1]
        ]
        let retArr = flipColumn(arr:arr2d)
        assert(retArr == flipArr2d)
    }

    func testFlipColumn2(){
        let arr2d = [
            [0, 1, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 1, 0]
        ]
        let flipArr2d = [
            [0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 1, 1, 0]
        ]
        let retArr = flipColumn(arr:arr2d)
        assert(retArr == flipArr2d)
    }

    func testFixedSizeArray(){
        let arr2d = [[Int]](repeating:[Int](repeatElement(0, count: 10)), count: 20)
        let height = arr2d.count
        let width = arr2d[0].count
        assert(height == 20)
        assert(width == 10)
    }

    static var allTests = [
        ("testExample", testExample),
        ("testFlipColumn1", testFlipColumn1),
        ("testFlipColumn2", testFlipColumn2),
        ("testFixedSizeArray", testFixedSizeArray),
        ]
}
