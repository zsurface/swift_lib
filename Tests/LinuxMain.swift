import XCTest

import swift_libTests

var tests = [XCTestCaseEntry]()
tests += swift_libTests.allTests()
XCTMain(tests)